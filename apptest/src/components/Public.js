import React, { useEffect, useRef, useState } from 'react';
import Keycloak from 'keycloak-js';
import { Config } from '../model/Config';
import { BrowserRouter, Route, Routes, Navigate, Outlet } from 'react-router-dom';
import Protected from './Protected';
import {useKeycloak} from "@react-keycloak/web"
const Home = () => {
    const {Keycloak}=useKeycloak();
    const {isAuthenticated,setIsAuthenticated}=useState(false)
    useEffect(()=>{
        if (Keycloak.isAuthenticated){
            setIsAuthenticated(true)
        }
    },[Keycloak.isAuthenticated])
    return (
        <div>
            <BrowserRouter>
                <Routes>
                    <Route path="/" element={<HomeContent  />} />
                    
                    <Route path='/auth' element={<Protected  islogin={isAuthenticated}/>} />
                    
                </Routes>
            </BrowserRouter>
        </div>
    );
};


const HomeContent = ({ authenticateLogin }) => {
    return (
        <div>
            <h3>Keycloak - OIDC</h3>
            <h4>React JS - Authorization Code Flow authentication</h4>
            <button className="btn-normal" onClick={authenticateLogin}>Login</button>
        </div>
    );
};

export default Home;
