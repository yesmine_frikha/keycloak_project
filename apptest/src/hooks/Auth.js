import { useState ,useEffect ,useRef } from 'react'
import keycloak from "keycloak-js"; 



const  useAuth =()=>  {
    const [islogin, setlogin] =useState(false)
    //create instance of keycklock 
    
    const isRun = useRef(false)
    useEffect(()=> {
     if (isRun.current) return ;
     isRun.current= true ;
     let initOptions = {
      url: process.env.REACT_APP_KEYCLOCK_URL,
      realm: process.env.REACT_APP_KEYCLOCK_REALM,
      clientId: process.env.REACT_APP_KEYCLOCK_CLIENTID,
      onLoad: 'login-required',
      KeycloakResponseType: 'code',
    }
        
    
    const client = new keycloak(initOptions);
    client.init({ onLoad: initOptions.onLoad, KeycloakResponseType: 'code' }).then((res) => setlogin(res))
  },[])
    return (
      islogin
    )
  } ;


export default useAuth
